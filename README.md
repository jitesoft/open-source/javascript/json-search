# json-search


Package implementing easy search of json data using query builder and constraints.  
Influenced by Laravel Eloquent in query building and HCL in path writing.

_This package is a work in progress._
