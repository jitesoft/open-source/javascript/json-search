const Path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  mode: 'production',
  target: 'web',
  entry: {
    'index': [
      Path.join(__dirname, 'src', 'index.js')
    ]
  },
  node: {
    global: true,
    process: false,
    Buffer: false
  },
  output: {
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'json-search',
    sourceMapFilename: '[name].map.js',
    globalObject: "typeof self !== 'undefined' ? self : this"
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(Path.join(__dirname, '/dist'))
  ],
  optimization: {
    minimize: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        include: [Path.join(__dirname, 'src')],
        loader: 'babel-loader'
      }
    ]
  }
};
