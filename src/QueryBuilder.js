import Insert from './QueryStatements/Insert';
import Delete from './QueryStatements/Delete';
import Update from './QueryStatements/Update';
import Select from './QueryStatements/Select';

export default class QueryBuilder {
  /**
   * Create a new Select statement to query JSON data with.
   *
   * @param {Array<string>|null} keys - Keys to select from the queried data.
   * @return {Select}
   */
  static select (keys = null) {
    return new Select(keys);
  }

  static update (statement) {
    return new Update(statement);
  }

  static delete (statement) {
    return Delete(statement);
  }

  static insert (statement) {
    return new Insert(statement);
  }
}
