/**
 * @typedef Constraint~Test(obj: Object): bool;
 */

export default class Constraint {
  #test;

  /**
   * @param {Constraint~Test} test - Function which will run on each of the objects.
   */
  constructor (test) {
    this.#test = test;
  }

  /**
   * @internal
   * @return {Constraint~Test}
   */
  get test () {
    return this.#test;
  }
}
