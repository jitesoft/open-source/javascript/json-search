export default class AbstractStatement {
  /** @internal */
  _data;
  /** @internal */
  _constraints = [];
  /** @internal */
  _path;

  /**
   * Set the data to query.
   * @param {Object|Array<*>} data - Data to query.
   * @param {String} path - Path to the object/s to query for.
   */
  in (data, path) {
    this._data = data;
    this._path = path;
    return this;
  }

  /**
   * Set the data to query.
   * @param {Object|Array<*>} data - Data to query.
   * @param {String} path - Path to the object/s to query for.
   * @alias in
   */
  from (data, path) {
    return this.in(data, path);
  }

  /**
   * @param {Array<Constraint>} constraints Constraints to use when searching.
   * @throws Error - If `in` is not called beforehand.
   */
  where (constraints = []) {
    if (!this._data) {
      throw new Error('Data to query not set. Please call `in(...)` before `where`.');
    }

    this._constraints = constraints;
    return this;
  }

  /**
   * @abstract
   * @return Promise<Object|Array<*>>
   * @throws Error
   */
  execute () {
    throw new Error('Called an abstract method.');
  }

  /**
   * Iterate a json object until the tokens list reaches 0.
   * Will return an array of results.
   *
   * @param {Array|Object} data - Data to iterate.
   * @param {Array<String>} tokens - List of tokens to use for iteration.
   * @return {Array} List of objects at the leaf.
   * @protected
   * @throws Error If token is miss-matched.
   */
  _iterate (data, tokens) {
    if (tokens.length === 0) {
      return [ data ]; // Return the data as an array as it will be inserted into the result as one.
    }

    const token = tokens.pop();
    const result = [];

    if (token === '*') {
      if (!Array.isArray(data) || typeof data !== 'object') {
        throw Error(`Failed to iterate structure. A '*' token was encountered, but the data point was neither an array nor object.`);
      }
      for (let p of data) {
        // slice is to "clone" the array shallowly so that each iteration gains the same but cloned array of tokens.
        result.push(...this._iterate(p, tokens.slice(0)));
      }
    } else {
      if (!(token in data)) {
        throw Error(`Failed to iterate structure. A named token ('${token}') was encountered, but the data point did not have said field.`);
      }

      result.push(...this._iterate(data[token], tokens));
    }

    return result;
  }

}
