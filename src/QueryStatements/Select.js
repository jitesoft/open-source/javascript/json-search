import AbstractStatement from './AbstractStatement';

export default class Select extends AbstractStatement {
  #keys;

  constructor (keys = null) {
    super();
    this.#keys = keys;
  }

  execute () {
    // Traverse the path.
    const tokens = this._path.split('.').reverse();
    const results = this._iterate(this._data, tokens);

    // Apply each constraint on the objects and return them.
    let out = results.filter((result) => {
      return !this._constraints.some((constraint) => {
        return !constraint.test(result);
      });
    });

    if (this.#keys && this.#keys.length > 0) {
      out = out.map((o) => {
        const res = {};
        for (let v of this.#keys) {
          res[v] = (v in o) ? o[v] : null;
        }
        return res;
      });
    }

    if (this._path.indexOf('*') !== -1) {
      return out.flat();
    }
    return out.length > 0 ? out[0] : null;
  }
}
