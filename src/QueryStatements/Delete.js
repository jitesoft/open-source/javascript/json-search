import AbstractStatement from './AbstractStatement';

export default class Delete extends AbstractStatement {
  execute () {
    return undefined;
  }

  explain () {
    return '';
  }
}
