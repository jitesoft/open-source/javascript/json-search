import AbstractStatement from './AbstractStatement';

export default class Update extends AbstractStatement {
  execute () {
    return undefined;
  }

  explain () {
    return '';
  }
}
