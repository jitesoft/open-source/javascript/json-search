import QueryBuilder from './QueryBuilder';

export default QueryBuilder;

const select = QueryBuilder.select;
const update = QueryBuilder.update;
const del = QueryBuilder.delete;
const insert = QueryBuilder.insert;

export {
  QueryBuilder,
  select,
  update,
  insert,
  del
};
