import data from './testdata';
import { select } from '../src/index';

describe('Tests for Select statement.', () => {
  const mixedData = data.misc;
  const stringData = data.string[0];
  const numberData = data.number;

  describe('Test selecting string value.', () => {
    test('String value, depth 1.', () => {
      const result = select().in(stringData, 'shadow').execute();
      expect(result).toBe('love');
    });

    test('String value, depth 3.', () => {
      const result = select().in(stringData, 'above.0.thick').execute();
      expect(result).toBe('climate');
    });

    test('String value, depth 6, array result.', () => {
      const result = select().in(stringData, 'above.0.characteristic.1.0.giant').execute();
      expect(result).toEqual(['settlers', 'regular', 'threw']);
    });
  });

  describe('Test selecting numbreric value.', () => {
    test('Number value, depth 1.', () => {
      const result = select().in(numberData, '2').execute();
      expect(result).toEqual(266325295.6926782);
    });

    test('Number value, depth 3.', () => {
      const result = select().in(numberData, '0.drop.1').execute();
      expect(result).toEqual(-1140720571.4610102);
    });

    test('Number value, depth 7, array result.', () => {
      const result = select().in(numberData, '0.drop.0.2.10.2.guard').execute();
      expect(result).toEqual([
        -1676079357,
        1388972599.8586895,
        -1162376926.0188015
      ]);
    });
  });

  describe('Test selecting object value.', () => {
    test('Simple, depth 7.', () => {
      const result = select().in(mixedData, 'tape.curious.2.3.0.0.something').execute();
      expect(result).toEqual({
        'tall': true,
        'enter': 1159653404,
        'situation': -2095969449.2466316,
        'mad': 'gather'
      });
    });

    test('Selected values, depth 7.', () => {
      const result = select(['tall', 'mad']).in(mixedData, 'tape.curious.2.3.0.0.something').execute();
      expect(result).toEqual({
        'tall': true,
        'mad': 'gather'
      });
    });
  });

  describe('Test select multiple results.', () => {
    test('String value, depth 1.', () => {
      const result = select([]).from(mixedData, 'rust.*').execute();
      expect(result).toEqual(expect.arrayContaining([
        'apple', 'banana', 'orange', 'pear', 'bernerna'
      ]));
      expect(result).toHaveLength(5);
    });
  });

  describe('Test selecting none-existing value.', () => {
    test('Invalid key', () => {
      const query = select().in(mixedData, 'tape.apa');
      expect(() => query.execute()).toThrowError('Failed to iterate structure. A named token (\'apa\') was encountered, but the data point did not have said field.');
    });

    test('Not an array.', () => {
      const query = select().in(mixedData, 'tape.*');
      expect(() => query.execute()).toThrowError('Failed to iterate structure. A \'*\' token was encountered, but the data point was neither an array nor object.');
    });
  });
});
